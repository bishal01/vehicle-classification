import os
import numpy as np
import pandas as pd
import scipy
import sklearn
import keras
from imutils import paths
from keras.models import Sequential
from keras.models import model_from_json
from keras.preprocessing.image import ImageDataGenerator
from keras.layers import Dense, Dropout, Activation, Flatten
import cv2
import tensorflow as tf
from skimage import io
from keras.layers import Conv2D, MaxPooling2D
import random
import matplotlib.pyplot as plt
from skimage.io import imread


# load json and create model
json_file = open('myweights2.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
model = model_from_json(loaded_model_json)
# load weights into new model
model.load_weights("weights3.inception.hdf5")
print("Loaded model from disk")
train_datagen = ImageDataGenerator(rescale = 1./255,
                                   shear_range = 0.2,
                                   zoom_range = 0.2,
                                   horizontal_flip = True)

training_set = train_datagen.flow_from_directory('./Machine Learning/vehicle-detection-python/train',
                                                 target_size = (200, 200),
                                                 batch_size = 17,
                                                 shuffle="true",
                                                 class_mode = "binary")


#-------------------------------------------------------------------------

filepath="./Machine Learning/vehicle-detection-python/test - Copy"
TEST_FILE="./Machine Learning/testfile.txt"


test_datagen = ImageDataGenerator(rescale=1./255)

test_generator = test_datagen.flow_from_directory(
        filepath,
        target_size=(200, 200),
        color_mode="rgb",
        shuffle = False,
        class_mode='categorical',
        batch_size=1)

filenames = test_generator.filenames
nb_samples = len(filenames)
probabilities  = model.predict_generator(test_generator,steps = nb_samples)
for index, probability in enumerate(probabilities):
    image_path = filepath + "/" +filenames[index]
    result = model.predict_generator(test_generator, nb_samples)
    img = imread(image_path)
    with open(TEST_FILE,"a") as fh:
        fh.write(str(probability[0]) + " for: " + image_path + "\n")
    plt.imshow(img)
    if probability > 0.5:
        plt.title("%.2f" % (probability[0]*100) + "% vehicle")
    else:
        plt.title("%.2f" % ((1-probability[0])*100) + "% not-vehicle")

    plt.show()
    


