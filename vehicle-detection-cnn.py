import os
import numpy as np
import pandas as pd
import scipy
import sklearn
import keras
from imutils import paths
from keras.models import model_from_json
from keras.preprocessing.image import ImageDataGenerator
import cv2
import tensorflow as tf
from skimage import io
import random
from skimage.io import imread
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.callbacks import ModelCheckpoint
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.metrics import confusion_matrix


classifier = Sequential()

classifier.add(Conv2D(32, (3, 3), input_shape = (200, 200, 3), activation = 'relu'))
classifier.add(MaxPooling2D(pool_size = (2, 2)))

classifier.add(Conv2D(32, (3, 3), activation = 'relu'))
classifier.add(MaxPooling2D(pool_size=(2, 2)))

classifier.add(Conv2D(64, (3, 3), activation = 'relu'))
classifier.add(MaxPooling2D(pool_size=(2, 2)))

classifier.add(Flatten()) #

classifier.add(Dense(units = 64, activation = 'relu')) #all connected

classifier.add(Dropout(0.5))

# output layer
classifier.add(Dense(1))
classifier.add(Activation('sigmoid')) #on output layer

checkpointer = ModelCheckpoint(filepath='./Machine Learning/vehicle-detection-python/weights4.inception.hdf5', verbose=1, save_best_only=True)


classifier.compile(optimizer = 'sgd', loss = 'binary_crossentropy', metrics = ['binary_accuracy']) #global & local minima 


from keras.preprocessing.image import ImageDataGenerator

train_datagen = ImageDataGenerator(rescale = 1./255,
                                   shear_range = 0.2,
                                   zoom_range = 0.2,
                                   horizontal_flip = True)

test_datagen = ImageDataGenerator(rescale = 1./255)

training_set = train_datagen.flow_from_directory('./Machine Learning/vehicle-detection-python/train',
                                                 target_size = (200, 200),
                                                 batch_size = 32,
                                                 shuffle="true",
                                                 class_mode = "binary")

test_set = test_datagen.flow_from_directory('./Machine Learning/vehicle-detection-python/test',
                                            target_size = (200, 200),
                                            batch_size = 32,
                                            shuffle="true",
                                            class_mode = "binary")

history = classifier.fit_generator(training_set,
                         steps_per_epoch = 10,
                         epochs = 50,
                         validation_data = test_set,
                         validation_steps = 100,
                        callbacks=[checkpointer], shuffle=True)
test_steps = test_set.n // 32 # batch size

#classifier.load_weights('./Machine Learning/vehicle-detection-python/myweights.hdf5')
# serialize model to JSON
model_json = classifier.to_json()
with open("myweights2.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
classifier.save_weights("myweightsold.h5")
print("Saved model to disk")


classifier.load_weights('./Machine Learning/vehicle-detection-python/weights4.inception.hdf5')
classifier.evaluate_generator(test_set, verbose=1, steps=test_steps)

# summarize history for accuracy
plt.plot(history.history['binary_accuracy'] )
plt.plot(history.history['val_binary_accuracy'])
plt.title('model accuracy  ')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.show()


# summarize history for loss
plt.plot(history.history['loss'] )
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.show()


